my_list = ["Evander", 22, True]
print(my_list)

favorite_foods = []
food = input("What is one of your favorite foods? ")
favorite_foods.append(food)

food = input("Last time, one more favortie food? ")
favorite_foods.append(food)

print("Your fav foods are", favorite_foods)

num_foods = len(favorite_foods)
print("That's", num_foods, "of your favorite foods.")

days_of_week = [
  "Sunday",       # Index 0
  "Monday",       # Index 1
  "Tuesday",      # Index 2
  "Wednesday",    # Index 3
  "Thursday",     # Index 4
  "Friday",       # Index 5
  "Saturday",     # Index 6
]

print("The fist day of the week is", days_of_week[0])

days_of_week[3] = "Hump day!"
print(days_of_week)

days_of_week[5] = "FriYAY!"
print(days_of_week)